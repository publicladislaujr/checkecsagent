import smtplib

def envia_email (mensagem, on, off):
    try:
        sender='no-reply@email.com'
        receivers='email@email.com'
        message= '\n' + ' - Agentes OK : ' + str(on) + '\n' + ' - Agentes OFF : ' + str(off) +'\n' +str(mensagem) 
        smtpObj = smtplib.SMTP('serveremail.com',25)
        smtpObj.sendmail(sender, receivers, 'Subject: Status agente ECS nas instancias dos Cluster\n \n{}'.format(message))            
        smtpObj.quit() 
    except ValueError:
        print('Erro ao enviar e-mail')