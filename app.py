import json
from multiprocessing import Event
import boto3
from botocore.session import Session
from botocore.config import Config
from botocore.exceptions import ClientError
import enviamail as mandaemail
import time

sessao = Session()
region = 'sa-east-1','us-east-1'

try:   
    resultado=[]     
    qtagon = 0
    qtagoff = 0

    for rg in region:
        rs1=[] 
        sesecs = sessao.create_client('ecs', region_name=rg, config=Config(connect_timeout=5, read_timeout=60, retries={'max_attempts': 2}))  
        sesssm = sessao.create_client('ssm', region_name=rg, config=Config(connect_timeout=5, read_timeout=60, retries={'max_attempts': 2}))  
        sesec2 = sessao.create_client('ec2', region_name=rg, config=Config(connect_timeout=5, read_timeout=60, retries={'max_attempts': 2}))  
        response = sesecs.list_clusters()
        r1= response['clusterArns']

        for i in r1:
            ii=i.split('/')    
            rs1.append(ii[1])

        for i in rs1:
            response2 = sesecs.list_container_instances(cluster=i)
            r2= response2['containerInstanceArns'] 

            for x in r2:       
                valida= x.count('/')      

                if valida > 1 :
                    xx=x.split('/', 2)         
                    rs2=xx[2]                   
                    response3=sesecs.describe_container_instances(cluster=i, containerInstances=[rs2])
                    stag=response3['containerInstances'][0]['agentConnected']
                    ec2id=response3['containerInstances'][0]['ec2InstanceId']     

                    if stag == True :
                        qtagon = qtagon + 1
                        resultado.append( 'CLUSTER: '+i+' AWSINSTANCE: ' +ec2id+' AGENTE STATUS ' +" ON "+ 'REGIAO: ' +rg + '\n')                                                            
                    else :
                        qtagoff = qtagoff + 1                     
                        resultado.append( 'CLUSTER: '+i+' - AWSINSTANCE: ' +ec2id+' - AGENTE STATUS ' +" DESLIGADO "+ ' - REGIAO: ' +rg + '\n')                          
                        try:
                            responsessm = sesssm.send_command(InstanceIds=[ec2id], DocumentName='AWS-RunShellScript', Comment='Restart ECS-Agent', Parameters={'commands':['docker start ecs-agent']})
                            command_id = responsessm['Command']['CommandId']
                            time.sleep(2)
                            output = sesssm.get_command_invocation(CommandId=command_id,InstanceId=ec2id)
                            print(output)                               
                        except ClientError as e:
                            print(e)

                else :
                    continue

    resultfin=''.join([str(item) for item in resultado])
    if qtagoff > 0 :
        mandaemail.envia_email(resultfin,qtagon,qtagoff)

    print(' Servidores ON: '+ str(qtagon) + ' Servidores OFF: ' + str(qtagoff) + '\n' + str(resultfin))   



except Exception as e:
    print(e)
    mandaemail.envia_email('#QUEBROU#',99999,99999)

 

